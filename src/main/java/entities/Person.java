package entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class Person {

    @Id
    private Long id;

    private String name;

    private String surname;



    private Date date_of_birt;

    public Person(){}

    public Person(String name, String surname,Date date_of_birth) {
        this.name = name;
        this.surname = surname;
        this.date_of_birt = date_of_birth;
    }

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    public Date getDate_of_birt() {
        return date_of_birt;
    }

    public void setDate_of_birt(Date date_of_birt) {
        this.date_of_birt = date_of_birt;
    }


}
