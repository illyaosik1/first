package entities;

import entities.Contracts.AssistContract;
import entities.Contracts.CleanContract;
import entities.Contracts.GoalContract;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "Player")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Player extends Person {


    public enum TypeOfPlayer{Forward,Winger,Defender,Goalkeeper}

    private TypeOfPlayer typeOfPlayer;

    private Team team; //Composition

    @OneToOne(mappedBy = "player", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Contract contract;

    public Player(){}

    public Player(String name, String surname, Date date_of_birth) {
        super(name, surname, date_of_birth);
    }

    @ManyToOne
    protected Team getTeam(){
        return team;
    }


    protected void setTeam(Team team){
        this.team = team;
    }

    public void addTeam(Team team) throws Exception { //put checks
        for(Team t : Team.getTeams()){
                if(!t.getPlayers().contains(this)){
                    this.team = team;
                    team.getPlayers().add(this);
                }else {
                    throw new Exception("This player is already in a team, couldn't add to the team");
                }
        }
    }

    public void removeTeam() throws Exception { //put checks
        if(this.team!=null) {
            team.getPlayers().remove(this);
            this.team = null;
        }else {
            throw new Exception("Player doesn't belong to a team");
        }
    }

    @Enumerated
    protected TypeOfPlayer getTypeOfPlayer() {
        return typeOfPlayer;
    }

    public void setTypeOfPlayer(TypeOfPlayer typeOfPlayer){
        this.typeOfPlayer = typeOfPlayer;
    }

    @OneToOne
    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }
}
