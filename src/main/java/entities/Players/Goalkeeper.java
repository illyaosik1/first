package entities.Players;

import entities.Player;

import javax.persistence.Basic;
import javax.persistence.Entity;
import java.sql.Date;

@Entity(name = "Goalkeeper")
public class Goalkeeper extends Player {
    private int clean_matches = 0;



    public Goalkeeper(){}

    public Goalkeeper(String name, String surname, Date date_of_birth){
        super(name, surname, date_of_birth);
        super.setTypeOfPlayer(TypeOfPlayer.Goalkeeper);
    }

    @Basic
    public int getClean_matches() {
        return clean_matches;
    }

    public void setClean_matches(int clean_matches) {
        this.clean_matches = clean_matches;
    }
}
