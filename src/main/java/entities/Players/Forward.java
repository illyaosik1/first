package entities.Players;

import entities.Player;

import javax.persistence.Basic;
import javax.persistence.Entity;
import java.sql.Date;

@Entity(name = "Forward")
public class Forward extends Player {
    private int goals = 0;



    public Forward(){}

    public Forward(String name, String surname, Date date_of_birth){
        super(name, surname, date_of_birth);
        super.setTypeOfPlayer(TypeOfPlayer.Forward);
    }

    @Basic
    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }


}
