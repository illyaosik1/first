package entities.Players;

import entities.Contracts.AssistContract;
import entities.Player;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;


@Entity(name = "Winger")
public class Winger extends Player {
    private int goals = 0;


    public Winger(){}

    public Winger(String name, String surname, Date date_of_birth){
        super(name, surname, date_of_birth);
        super.setTypeOfPlayer(TypeOfPlayer.Winger);
    }

    @Basic
    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

}
