package entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Team {

    private static final List<Team> teams = new ArrayList<Team>();

    @Id
    private Long id;

    private List<Coach> coaches = new ArrayList<Coach>();
    private List<Player> players = new ArrayList<Player>();//composition

    private String name;

    public Team(String name){
        this.name = name;
        teams.add(this);
    }

    public Team(){}


    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    protected Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    @Basic
    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    @OneToMany(
            mappedBy = "team",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    protected List<Coach> getCoaches() {
        return coaches;
    }

    protected void setCoaches(List<Coach> coaches) {
        this.coaches = coaches;
    }

    public void addCoach(Coach coach){ //put checks
        getCoaches().add(coach);
        coach.setTeam(this);
    }

    public void removeCoach(Coach coach){ //put checks
        getCoaches().remove(coach);
        coach.setTeam(null);
    }

    @OneToMany(
            mappedBy = "team",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    protected List<Player> getPlayers() {
        return players;
    }

    protected void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player) throws Exception { //put checks
        for(Team t : Team.getTeams()){
            if(t.getPlayers().contains(this)){
                throw new Exception("Player is already has a team");
            }else {
                getPlayers().add(player);
                player.setTeam(this);
            }
        }
    }

    public void removePlayer(Player player) throws Exception { //put checks
        if(player.getTeam()==this) {
            getPlayers().remove(player);
            player.setTeam(null);
        }else {
            throw new Exception("Player doesn't have a team or in another team");
        }
    }

    public static List<Team> getTeams() {
        return teams;
    }
}
