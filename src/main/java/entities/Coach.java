package entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity(name = "Coach")
public class Coach extends Person {

    public Coach() {
    }

    private List<String> prizes;  //multi-value attribute

    public enum TypeOfCoach{HeadCoach, GoalkeeperCoach, WingerCoach, ForwardCoach, DefenderCoach}  //which players he trains

    private TypeOfCoach typeOfCoach;

    private Team team;

    public Coach(String name, String surname, Date date_of_birth){
        super(name,surname,date_of_birth);
    }

    @ManyToOne
    protected Team getTeam() {
        return team;
    }

    protected void setTeam(Team team) {
        this.team = team;
    }

    public void addTeam(Team team){
        this.team = team; //put checks
        team.getCoaches().add(this);
    }

    public void removeTeam(){  //put checks
        this.team = null;
        team.getCoaches().remove(this);
    }

    @Enumerated
    protected TypeOfCoach getTypeOfCoach(){
        return typeOfCoach;
    }

    public void setTypeOfCoach(TypeOfCoach typeOfCoach){
        this.typeOfCoach = typeOfCoach;
    }

    @ElementCollection
    public List<String> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<String> prizes) {
        this.prizes = prizes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Coach: %s, %s, %s",getName(),getSurname(),getDate_of_birt()));
        return sb.toString();
    }
}
