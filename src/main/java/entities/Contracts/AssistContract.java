package entities.Contracts;

import entities.Contract;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "AssistContract")
@Inheritance
public class AssistContract extends Contract {

    private int money_for_assist;






    public AssistContract(){}

    public AssistContract(Date start, Date end, int money_for_assist, int salary) throws Exception {
        super(start,end,salary);
        if(money_for_assist <= Contract.getMaxBonus()) {
            this.money_for_assist = money_for_assist;
        }else {
            throw new Exception("Bonus for assist is very high");
        }
    }

    @Basic
    public int getMoney_for_assist() {
        return money_for_assist;
    }

    public void setMoney_for_assist(int money_for_assist) throws Exception {
        if(money_for_assist <= Contract.getMaxBonus()) {
            this.money_for_assist = money_for_assist;
        }else {
            throw new Exception("Bonus for assist is very high");
        }
    }


}
