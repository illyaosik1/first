package entities.Contracts;

import entities.Contract;

import javax.persistence.Basic;
import javax.persistence.Entity;
import java.util.Date;

@Entity(name = "CleanContract")
public class CleanContract extends Contract {

    private int money_for_clean_match;





    public CleanContract(){}

    public CleanContract(Date start, Date end, int money_for_clean_match, int salary) throws Exception {
        super(start,end, salary);
        if(money_for_clean_match <= Contract.getMaxBonus()) {
            this.money_for_clean_match = money_for_clean_match;
        }else {
            throw new Exception("Bonus is very high");
        }

    }


    @Basic
    public int getMoney_for_clean_match() {
        return money_for_clean_match;
    }

    public void setMoney_for_clean_match(int money_for_assist) throws Exception {
        if(money_for_assist <= Contract.getMaxBonus()){
        this.money_for_clean_match = money_for_assist;
        }else {
            throw new Exception("Bonus is very high");
        }
    }
}
