package entities.Contracts;

import entities.Contract;

import javax.persistence.Basic;
import javax.persistence.Entity;
import java.util.Date;

@Entity(name = "GoalContract")
public class GoalContract extends Contract {

    private int money_for_goal;



    public GoalContract(){}

    public GoalContract(Date start, Date end, int money_for_goal, int salary) throws Exception {
        super(start,end, salary);
        if(money_for_goal <= Contract.getMaxBonus()) {
            this.money_for_goal = money_for_goal;
        }else {
            throw new Exception("Bonus is very high");
        }

    }


    @Basic
    public int getMoney_for_goal() {
        return money_for_goal;
    }

    public void setMoney_for_goal(int money_for_goal) throws Exception {
        if(money_for_goal <= Contract.getMaxBonus()) {
            this.money_for_goal = money_for_goal;
        }else {
            throw new Exception("Bonus is very high");
        }
    }
}
