package entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Contract")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Contract {
    @Id
    private int id;


    private static final int maxBonus = 200;  //class attribute

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Player player;

    private int salary;

    private java.util.Date start;
    private Date end;

    public Contract(){}

    public Contract(Date start, Date end, int salary){
        this.start = start;
        this.end = end;
        this.salary = salary;
    }


    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    public java.util.Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    @Basic
    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @OneToOne
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Basic
    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public static void signContract(Player player, Date start, Date end, int price){

    }


    public static int getMaxBonus() {
        return maxBonus;
    }

}
