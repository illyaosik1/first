import entities.Coach;
import entities.Player;
import entities.Players.Forward;
import entities.Players.Winger;
import entities.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.Date;

public class Main {
    public static void main(String[] args) throws Exception {
        Team team = new Team("Juventus");

        Player player = new Winger("Illia","Osiyuk",Date.valueOf("1996-11-10"));
        Player forward = new Forward("Leonel","Messi",Date.valueOf("1996-11-10"));
        Coach coach = new Coach("Pep","Guardiola",Date.valueOf("1979-12-1"));
        coach.setTypeOfCoach(Coach.TypeOfCoach.HeadCoach);

        team.addCoach(coach);
        team.addPlayer(player);
        team.addPlayer(forward);



        Session session = HibernateUtil.sessionFactory.openSession();
        session.beginTransaction();
        session.save(player);
        session.save(forward);
        session.save(coach);
        session.save(team);
        session.getTransaction().commit();
        session.close();
    }
}